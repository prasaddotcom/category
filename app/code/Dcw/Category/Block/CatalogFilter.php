<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Dcw\Category\Block;

/**
 * Dcw Deals block
 *
 * @author      Dcw Team 
 */
class CatalogFilter extends \Magento\Framework\View\Element\Template
{
    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array                                            $data
     */
    protected $layerState;

    public function __construct(
         
        \Magento\Catalog\Model\Layer\State $layerState
         
    ) {
        
         $this->layerState = $layerState;
        
    }

    public function getActiveCustomFilters()
    {
        return $this->layerState->getFilters();
    }


}


